package model;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class SimpleBinaryTreeMap<K extends Comparable, V> implements Map<K, V> {

  private int size = 0;
  private Node<K, V> root;


  private static class Node<K, V> implements Map.Entry<K, V> {

    private Node<K, V> left;
    private Node<K, V> right;
    private K key;
    private V value;

    public Node(K key, V value) {
      this.key = key;
      this.value = value;
    }

    @Override
    public K getKey() {
      return key;
    }

    @Override
    public V getValue() {
      return value;
    }

    @Override
    public V setValue(V value) {
      V oldValue = this.value;
      this.value = value;
      return oldValue;
    }
  }

  @Override
  public int size() {
    return size;
  }

  @Override
  public boolean isEmpty() {
    return size == 0 ? true : false;
  }

  @Override
  public boolean containsKey(Object key) {
    return false;
  }

  @Override
  public boolean containsValue(Object value) {
    return false;
  }

  @Override
  public V get(Object key) {
    K keyToFind = (K) key;
    Node<K, V> node = root;
    while (node != null) {
      int cmp = keyToFind.compareTo(node.key);
      if (cmp < 0) {
        node = node.left;
      } else if (cmp > 0) {
        node = node.right;
      } else {
        return node.value;
      }

    }
    return null;
  }


  @Override
  public V put(K key, V value) {
    insert(key, value);
    size++;
    return null;
  }

  private void insert(K key, V value) {
    Node<K, V> newNode = new Node<>(key, value);
    newNode.key = key;

    newNode.value = value;
    if (root == null) {
      root = newNode;
    } else {
      Node<K, V> current = root;
      Node<K, V> parent;
      int cmp;
      cmp = key.compareTo(current.key);
      while (true) {
        parent = current;

        if (cmp < 0) {
          current = current.left;
          if (current == null) {
            parent.left = newNode;
            return;
          }
        } else {
          current = current.right;
          if (current == null) {
            parent.right = newNode;
            return;
          }
        }
      }
    }
  }

  @Override
  public V remove(Object key) {
    delete(key);
    size--;
    return null;
  }

  private boolean delete(Object key) {
    Node current = root;
    Node parent = root;
    K keyToCompare = (K) key;
    boolean isLeftChild = true;
    while (current.key != key) {
      parent = current;
      int cmp = keyToCompare.compareTo(current.key);
      if (cmp < 0) {
        isLeftChild = true;
        current = current.left;
      } else {

        isLeftChild = false;
        current = current.right;
      }

      if (current == null) {
        return false;
      }
    }
    if (current.left == null && current.right == null) {
      if (current == root) {
        root = null;
      } else if (isLeftChild) {
        parent.left = null;
      } else {
        parent.right = null;
      }
    } else if (current.right == null) {
      if (current == root) {
        root = current.left;
      } else if (isLeftChild) {
        parent.left = current.left;
      } else {
        parent.right = current.left;
      }
    } else if (current.left == null) {
      if (current == root) {
        root = current.right;
      } else if (isLeftChild) {
        parent.left = current.right;
      } else {
        parent.right = current.right;
      }
    } else {
      Node<K, V> successor = getSuccessor(current);
      if (current == root) {
        root = successor;
      } else if (isLeftChild) {
        parent.left = successor;
      } else {
        parent.right = successor;
      }

      successor.left = current.left;
    }
    return true;
  }


  private Node<K, V> getSuccessor(Node<K, V> delNode) {
    Node successorParent = delNode;
    Node successor = delNode;
    Node current = delNode.right;
    while (current != null) {
      successorParent = successor;
      successor = current;
      current = current.left;
    }
    if (successor != delNode.right) {
      successorParent.left = successor.right;
      successor.right = delNode.right;
    }
    return successor;
  }

  public void print() {
    Node<K, V> localRoot = root;
    printInOrder(localRoot);
  }

  private void printInOrder(Node<K, V> localRoot) {
    if (localRoot != null) {
      printInOrder(localRoot.left);
      System.out.print(localRoot.value + " ");
      printInOrder(localRoot.right);
    }
  }

  @Override
  public void putAll(Map<? extends K, ? extends V> m) {

  }

  @Override
  public void clear() {

  }

  @Override
  public Set<K> keySet() {
    return null;
  }


  @Override
  public Collection<V> values() {
    return null;
  }

  @Override
  public Set<Entry<K, V>> entrySet() {
    return null;
  }
}
