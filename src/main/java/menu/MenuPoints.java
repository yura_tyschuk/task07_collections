package menu;

public enum MenuPoints {
  PUT(1), REMOVE(2), GET(3), PRINT(4), SIZE(5), ISEMPTY(5);

  private int menuNumber;

  private MenuPoints(int menuNumber) {
    this.menuNumber = menuNumber;
  }


}
