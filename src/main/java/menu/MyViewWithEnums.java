package menu;

import controller.Controller;
import model.SimpleBinaryTreeMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyViewWithEnums<K extends Comparable, V> {

  private Controller controller;
  private Map<String, String> menu;
  private MenuPoints menuPoints;
  private static Scanner input = new Scanner(System.in);
  SimpleBinaryTreeMap tmp;

  public MyViewWithEnums() {
    controller = new Controller();
    tmp = new SimpleBinaryTreeMap();
    menu = new LinkedHashMap<>();

    menu.put("1", "1 - Put a value to tree");
    menu.put("2", "2 - Remove a value from tree");
    menu.put("3", "3 - Get value from tree");
    menu.put("4", "4 - Print tree");
    menu.put("5", "5 - Get size of a tree");
    menu.put("6", "6 - Check if tree is empty");
    menu.put("Q", "Quit");

  }

  private void outputMenu() {
    System.out.println("\n Menu: ");
    for (String str : menu.values()) {
      System.out.println(str);
    }
  }

  private void putInTree() {
    System.out.println("Type key: ");
    K key = (K) input.nextLine();
    System.out.println("Type value: ");
    V value = (V) input.nextLine();
    controller.put(key, value);
  }

  private void deleteFromTree() {
    System.out.println("Type key to remove: ");
    K key = (K) input.nextLine();
    controller.remove(key);
  }

  private void printTree() {
    controller.print();
  }

  private void getFromTree() {
    System.out.println("Print key to get: ");
    Object key = (Object) input.nextLine();
    controller.get(key);
  }

  private void getSizeOfTree() {
    System.out.println(controller.size());
  }

  private void checkIfTreeIsEmpty() {
    System.out.println(controller.isEmpty());
  }

  public void show() {
    String keyMenu;
    do {
      outputMenu();
      System.out.println("Please select menu point: ");
      keyMenu = input.nextLine().toUpperCase();
      try {

      } catch (Exception e) {

      }
    } while (!keyMenu.equals("Q"));
  }


}