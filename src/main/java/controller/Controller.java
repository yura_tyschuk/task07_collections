package controller;

import model.SimpleBinaryTreeMap;

public class Controller<K extends Comparable, V> {

  private SimpleBinaryTreeMap simpleBinaryTreeMap;

  public Controller() {
    simpleBinaryTreeMap = new SimpleBinaryTreeMap();
  }

  public V put(K key, V value) {
    simpleBinaryTreeMap.put(key, value);
    return null;
  }

  public V remove(Object key) {
    simpleBinaryTreeMap.remove(key);
    return null;
  }

  public void print() {
    simpleBinaryTreeMap.print();
  }

  public V get(Object key) {
    simpleBinaryTreeMap.get(key);
    return null;
  }

  public int size() {
    return simpleBinaryTreeMap.size();
  }

  public boolean isEmpty() {
    return simpleBinaryTreeMap.isEmpty();
  }

}
